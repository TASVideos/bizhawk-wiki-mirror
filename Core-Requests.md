This page documents various emulator core candidates and specific user requests for systems.

TODO merge in [Yoshi's list on GitLab](https://gitlab.com/TASVideos/BizHawk/-/snippets/1890492)

## (3DO) 4DO
- https://sourceforge.net/p/fourdo/code/HEAD/tree/trunk http://fourdo.com
- C# (net40); GPLv2

## (3DS) Citra
- https://github.com/citra-emu/citra https://citra-emu.org
- C++; GPLv2+
- Issues:
	- [#3180](https://github.com/TASEmulators/BizHawk/issues/3180)
- Notes:
	- Citra has its own rerecording tools and Citra movies are acceptable on TASVideos.

## (Amiga) FS-UAE or WinUAE
- https://github.com/FrodeSolheim/fs-uae https://fs-uae.net (fork of http://winuae.net)
- C/C++; GPLv2
- Issues:
	- [#2003](https://github.com/TASEmulators/BizHawk/issues/2003)

## (Atari 5200) Altirra or Atari800
Tracking as [#3317](https://github.com/TASEmulators/BizHawk/issues/3317).

## (BBC Micro) beebjit or BeebEm or B-Em
Tracking as [#2031](https://github.com/TASEmulators/BizHawk/issues/2031).

## (Cave Story engine) NXEngine
- https://nxengine.sourceforge.io (source distributed as archives)
- C++; GPLv3

## (Coleco Adam) MAME subset
- can likely copy most Hawk-side code from MAME Arcade core

## (Dreamcast) Flycast or Reicast
Tracking as [#856](https://github.com/TASEmulators/BizHawk/issues/856).

## (Flash) Ruffle
Tracking as [#2032](https://github.com/TASEmulators/BizHawk/issues/2032).

## (IBM PC) 86Box or DOSBox

### 86Box
- https://github.com/86Box/86Box (fork of https://www.pcem-emulator.co.uk/)
- C/C++; GPLv2+
- Issues:
	- [#1673](https://github.com/TASEmulators/BizHawk/issues/1673)
- Notes
	- Standalone program seeing use for TASing already, see [these](https://tasvideos.org/Forum/Topics/20206) two [threads](https://tasvideos.org/Forum/Topics/22316) on TASVideos, and [this thread](https://tasvideos.org/Forum/Topics/23002) for 86Box specifically.

### DOSBox
- https://github.com/schellingb/dosbox-pure (fork of https://dosbox.com/)
- C/C++; GPLv2+
- Issues:
	- [#2547](https://github.com/TASEmulators/BizHawk/issues/2547)
- Core Analysis
	- Early attempts (2010 or so) to add rerecording suggesting it is highly sync unstable.  Situation may have changed since then.  Waterbox highly recommended.
	- In particular, according to Ilari it had a lot of reentrancy with HLE operations, so you could get to a "frame end" while servicing an x86 BIOS call in HLE code, requiring the ability to save and load with different call sequences on the stack.

## (Pokémon mini) GB Enhanced+ or PokeMini
Tracking as [#3162](https://github.com/TASEmulators/BizHawk/issues/3162).

## (PS2) PCSX2
Tracking as [#3318](https://github.com/TASEmulators/BizHawk/issues/3318).

## (PSP) PPSSPP
- https://github.com/hrydgard/ppsspp https://ppsspp.org
- C/C++; GPLv2+
- Notes:
	- PPSSPP has been proof-of-concept added to Bizhawk before, but it was deemed too complicated and too early in development. However, that was quite a while ago.
	- Has a software rendering option which is fast enough; should probably go with that even if not Waterboxing.
	- There has been some success with running PPSSPP standalone in libTAS, see [this forum thread](https://tasvideos.org/Forum/Topics/20542).
- Other emulators:
	- [cspspemu](https://github.com/cspspemu/cspspemu) in C#, unfortunately targets .NET Core (can maybe use new AoT and exports features to make unmanaged core?)

## (Symbian OS) EKA2L1
Tracking as [#3364](https://github.com/TASEmulators/BizHawk/issues/3364).

## (Wii U) Cemu
- https://github.com/cemu-project/Cemu https://cemu.info/
- C/C++; MPL2
- Notes:
	- Recently (August 2022) published as Free Software. Linux port [very much a WIP](https://github.com/cemu-project/Cemu/issues/1).

## (X68000) Virtual X86000
- https://bitbucket.org/kazssym/libvm68k/src https://www.vx68k.org/vx68k
- C++; GPLv3+
- Other emulators:
	- [PX68K](https://github.com/hissorii/px68k) (in Japanese) in C/C++; no license, though the author granted RetroArch distribution rights
Note that some of these can be done concurrently.

1. Choose to merge or not merge pending changes
	- Ideally there would be a release candidate and rigorous testing, but there never seems to be enough time...
1. Test Issues labelled "Patch pending"
1. Write the changelog
	- You can use `git log --pretty='format:[%h %an] %s%n%b%n' 2.x..` (substitute the last release tag for "2.x") as a starting point.
	- After parsing through the commits to get a list of user-facing changes, group them as in the example below. Use past tense. You should include Issue numbers for bugs and feature requests, but not PRs.
	- This needs to be prepended to `/Assets/changelog.md` (Markdown) *immediately before* updating the `release` branch so that it ends up in the build artifacts.
	- It also needs to be prepended to [the `ReleaseHistory` article](https://tasvideos.org/Bizhawk/ReleaseHistory) (TASVideos Wiki markup). Apart from changing markup language, also remember to include download links and the release date (will need to finish this after publishing).
	- There's also [the ext. tool changelog](https://github.com/TASEmulators/BizHawk-ExternalTools/wiki/Partial-changelog-for-ApiHawk). If an ApiHawk change doesn't affect Lua then it might not warrant inclusion on the main changelog, but this one includes even the tiny method signature changes.
1. Update `VersionInfo.MainVersion` if it was set for a point release but this will be a minor release
1. Update the `release` branch to point to `master`
1. Check for AV false positives
	- Separately upload the release archive and just `EmuHawk.exe` to [VirusTotal](https://www.virustotal.com/gui/home/upload). A false positive from the Microsoft engine is the only one that really matters.
	- If it does happen... add a random `<EmbeddedResource/>` to the assembly or something and try again idk ¯\\\_(ツ)\_/¯
1. Create a "Release" on GitHub
	- Set it to create a new tag.
	- [GitLab CI](https://gitlab.com/TASVideos/BizHawk/-/pipelines?page=1&scope=all&ref=release) will build the `release` branch shortly after pushing it. Grab the `package_release_*` artifacts, replace "VERSIONHERE" in the filenames, recompress the Linux release as `.tar.gz`, and attach them to the Release.
	- For the description, list any new or graduating cores, optionally some large improvements, and if the Windows prereq installer was updated link that too.
	- Click "Publish" and pray.
1. Update Issues, Labels, and Milestone on GitHub
	- (where this release will be 2.x.1)
	- Rename "Fixed/added in 2.x.1 dev" to "Fixed/added in 2.x.1" (i.e. remove "dev"; do not rename "Affects 2.x.1 dev", these should be re-triaged).
	- Add "Affects 2.x.1", "Regression from 2.x.1", and "Affects 2.x.2 dev".
	- Add/update core labels
	- Create a new Milestone "2.x.2" with target date 3 months from this release.
	- Move some or all open Issues to the new Milestone. There's no policy for this as we don't really use it for project management.
	- Close the "2.x.1" Milestone.
	- If any Issue was critical enough to warrant pinning, but was fixed for this release, unpin it.
	- Re-triage Issues which were only reproduced on dev builds ("Repro: Affects 2.x.1 dev"), and Issues which affected cores that were updated in this release.
1. Update `VersionInfo.MainVersion` and `VersionInfo.ReleaseDate`
	- `ReleaseDate` is overwritten by CI for release builds, but needs to be checked-in. `MainVersion` should be set to e.g. `"2.x.2"` after releasing 2.x.1.
1. Update readme and other documentation pages
	- The version isn't explicitly written in any links or text in the readme, but the information itself might become outdated, especially the core list.
	- [LuaFunctions on TASVideos Wiki](https://tasvideos.org/BizHawk/LuaFunctions)
1. Nix stuff?
1. Hype
	- TASVideos news, Discord, etc.

### Changelog structure
Treat Libretro as just another core in this context.
The cores under "Other cores:" are in alphabetical order, new/graduating aren't.
The changes should be sorted with the most important first, but that's kinda subjective so don't try too hard.
Obviously, omit empty headings.
```
- Misc. changes to EmuHawk:
	- change
- Linux port:
	- change
- TAStudio:
	- change
- Lua/ApiHawk:
	- change
- DiscoHawk:
	- change
- New and graduating cores:
	- new core:
		- change
	- graduating core:
		- change
- Other cores:
	- a core:
		- change
	- another core:
		- change
```